﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDebugable
{
    void Show(string s);
}