﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Starting : MonoBehaviour
{
    void Start()
    {
        CustomDebuger cd = new CustomDebuger();
        Decorator_1 d1 = new Decorator_1(cd);
        Decorator_2 d2 = new Decorator_2(d1);
        d2.Show("Decorator_2");
    }
}