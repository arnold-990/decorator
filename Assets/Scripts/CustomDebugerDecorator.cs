﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomDebugerDecorator : IDebugable
{
    public CustomDebugerDecorator(IDebugable debugable) { Debugable = debugable; }
    protected IDebugable Debugable;

    public virtual void Show(string s)
    {
        Debug.Log(s.Length);
        Debugable.Show(s);
    }
}